(function ($) {
    "use strict";

    var $window = $(window),
        $document = $(document),
        $body = $('body');


    $document.ready(function() {
        /*Evaluates the copyright year*/
        var d = new Date();
        var n = d.getFullYear();
        $("#copyright-year").text(n);

        setTimeout(function() {
            $('.preloader').fadeOut(500);
        }, 500);
    });

}(jQuery));